const express = require('express');

// Created an application using express
// In layman's term, app is our server
const app = express();

const port = 4000;

	// Middleware is a software that provide common services and capabilities to applications outside of what's offered by the operating system.
	// use() -> is a method to configure the middleware used by the routes of exprses
	// epxress.json() -> allows your app to read json data
app.use(express.json());
	// By default, information received from the url can only be received as a string or an array.
	// By applying the "extended:true" inside the express.urlencoded, this allows us to receive information in other data types such as an object which we will use throughout our application.
app.use(express.urlencoded({ extended:true }));

// Mockdatabase
let users = [];

// Routes/endpoint
// http://localhost:4000/

// GET
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint")
})

// POST
app.post("/hello",(req,res) => {
	// req.body contains the contents/data of the request body.
	//
	res.send(`Hello there ${req.body.firstname} ${req.body.lastname}`)
})


// Create a POST route to register a user.

app.post("/signup",(req, res) => {
	console.log(req.body)
	// validation
	// If contents of the req.body with the property username and password is not empty then push the data to the users array. Else, please input both username and password.
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body)
		console.log(users) // to check if existing
		res.send(`User ${req.body.username} successfully registered.`)
	}
	else {
		res.send("Please input both username and password.")
	}
})

// This route expects to receive a PUT request at the URI "/change-password"
// Create a PUT
// This will update the password or a user that matches the informatino provided in the client.
app.put("/change-password", (req, res) => {

	let message;

	for (let i = 0; i < users.length; i++) {
		// If the username provided in the postman/client and the username of the current object in the loop is the same.
		if(req.body.username === users[i].username) {
			// change the password of the user found by the loop into the provided client.
			users[i].password = req.body.password

			// message response
			message = `User ${req.body.username}'s password has been updated.`;
			// Breaks out of the loop once a user matches the username provided.
			break;
		}
		else {
			message = "User does not exist."
		}

	}

	res.send(message);
})


// GET Home
app.get("/home",(req,res) => {

	res.send("Welcome to the Homepage.")
	
})

// GET Users
app.get("/users",(req,res) => {

	res.send(users)


})

// DELETE Users
app.delete("/delete-user",(req, res) => {

		

		for (let i = 0; i < users.length; i++) {
		if(req.body.username === users[i].username) {
			
			users.splice(0,1);
			console.log(users) // to check if deleted
			message = `User ${req.body.username} has been deleted.`;
			break;
		}
		else {
			message = "User does not exist."
		}

	}

	res.send(message);

})


// Returns a message to confirm that the server is running in the ternminal.
app.listen(port, () => console.log(`Server running at port ${port}`));


